#include "weatherapi.h"

#include <QNetworkAccessManager>
#include <QJsonObject>
#include <QJsonDocument>

class WeatherApiData
{
public:
    QString m_url;
    QNetworkAccessManager* m_networkManager;

};

WeatherApi::WeatherApi(QObject *parent)
    : QObject(parent)
{
    m_data = new WeatherApiData();
    m_data->m_networkManager = new QNetworkAccessManager(this);

    connect(m_data->m_networkManager, &QNetworkAccessManager::finished, this, &WeatherApi::replyFinished);
}

WeatherApi::~WeatherApi()
{
    delete m_data;
}

QString WeatherApi::url() const
{
    return m_data->m_url;
}

void WeatherApi::setUrl(const QString &url)
{
    if (m_data->m_url != url)
    {
        m_data->m_url = url;
        emit urlChanged();
    }
}

void WeatherApi::requestWeather(const QString& searchString)
{
    m_data->m_networkManager->get(QNetworkRequest(QUrl(m_data->m_url + searchString + "&APPID=" + AppID)));
}

void WeatherApi::replyFinished(QNetworkReply* reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        QJsonObject json = QJsonDocument::fromJson(reply->readAll()).object();
        emit resultFinished(json);
    }
}
