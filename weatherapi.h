#ifndef WEATHERAPI_H
#define WEATHERAPI_H

#include <QObject>
#include <QNetworkReply>

const QString AppID = "4f84d2a61b49deef538f2628db1a53e4";

class WeatherApiData;

class WeatherApi : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
public:
    explicit WeatherApi(QObject *parent = nullptr);
    ~WeatherApi();

    QString url() const;
    void setUrl(const QString& url);

signals:
    void urlChanged();
    void resultFinished(const QJsonObject& result);

public slots:
    void requestWeather(const QString& searchString);

private slots:
    void replyFinished(QNetworkReply* reply);

private:
    WeatherApiData* m_data;
};

#endif // WEATHERAPI_H
