import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12

import org.rzhevskiy.weather 1.0

ApplicationWindow {
    id: _root

    visible: true
    width: 480
    height: 800
    title: qsTr("Demo Weather App")
    color: "dodgerblue"

    WeatherApi {
        id: _weatherApi
        url: "http://api.openweathermap.org/data/2.5/weather?q="
        onResultFinished: {
            _cityNameText.text = result["name"]
            _descText.text = result["weather"][0]["main"]
            _tempText.text = Math.round(result["main"]["temp"] - 273.15) + " °C"
            _cloudText.text = "Cloudiness " + (result["clouds"]["all"]) + " %"

            _iconImage.source = "http://openweathermap.org/img/wn/" + result["weather"][0]["icon"] + "@2x.png"

             _tempParam.text = "Temperature: " + _tempText.text
            _humidityParam.text = "Humidity: " + result["main"]["humidity"] + " %"
            _pressureParam.text = "Pressure: " + result["main"]["pressure"] + " hPa"
            _windParam.text = "Wind speed: " + result["wind"]["speed"] + " m/s"
        }
    }

    Rectangle {
        id: _topBar

        color: "cornflowerblue"
        width: parent.width
        height: 50
        anchors.top: parent.top

        Rectangle {
            id: _newCityTextRect

            width: _newCityText.implicitWidth + 50
            height: _newCityText.implicitHeight + 10

            anchors {
                top: parent.top
                topMargin: 10
                left: parent.left
                leftMargin: 10
            }

            TextInput {
                id: _newCityText

                text: "Type in new city"
                font.pointSize: 18
                anchors.centerIn: parent

                onFocusChanged: {
                    selectAll()
                }
            }
        }

        Rectangle {
            width: _textButton.implicitWidth + 30
            height: 30
            radius: 15
            color: "deepskyblue"

            anchors {
                top: parent.top
                topMargin: 10
                right: parent.right
                rightMargin: 10
            }

            Text {
                id: _textButton
                anchors.centerIn: parent
                text: "Add City"
                font.pointSize: 18
                color: "white"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var name = _newCityText.text
                    _weatherModel.append({name: name, searchString: name + ",ru"})
                    _newCityText.text = "Type in new city"
                    _newCityText.focus = false
                }
            }
        }
    }

    ColumnLayout {
        id: _layout
        anchors {
            top: _topBar.bottom
            topMargin: 10
        }

        spacing: 10
        width: parent.width

        Text {
            id: _cityNameText

            color: "white"
            Layout.alignment: Qt.AlignCenter
            text: "Saint Petersburg"
            font {
                pointSize: 25
                weight: Font.Bold
                capitalization: Font.AllUppercase
            }
        }

        Image {
            id: _iconImage
            Layout.alignment: Qt.AlignCenter
            smooth: false
        }

        Text {
            id: _descText
            color: "white"
            Layout.alignment: Qt.AlignCenter
            text: "Description"
            font.pointSize: 20
        }

        Text {
            id: _tempText

            color: "white"
            Layout.alignment: Qt.AlignCenter
            text: "20 °C"
            font.pointSize: 50
        }

        Text {
            id: _cloudText

            color: "white"
            Layout.alignment: Qt.AlignCenter
            text: "80 %"
            font.pointSize: 20
        }

        Rectangle {
            id: _allParamsRect

            width: parent.width
            height: _allParametersColumn.implicitHeight

            Layout {
                alignment: Qt.AlignLeft
                leftMargin: 5
            }
            border.color: "darkblue"

            Column {
                id: _allParametersColumn

                width: parent.width
                anchors.centerIn: parent

                Text {
                    id: _tempParam
                    text: "Temp: "
                    color: "white"
                    font.pointSize: 20
                }

                Text {
                    id: _humidityParam
                    text: "Humidity: "
                    color: "white"
                    font.pointSize: 20
                }

                Text {
                    id: _pressureParam
                    text: "Pressure: "
                    color: "white"
                    font.pointSize: 20
                }

                Text {
                    id: _windParam
                    text: "Wind: "
                    color: "white"
                    font.pointSize: 20
                }
            }
        }
    }

    ListView {
        id: _listView

        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds

        width: parent.width

        anchors {
            top: _layout.bottom
            topMargin: 15
            bottom: parent.bottom
        }

        signal clicked(int index);
        onClicked: {
            _cityNameText.text = model.get(index).name;
            _weatherApi.requestWeather(model.get(index).searchString);
        }

        model: WeatherModel {
            id: _weatherModel
        }
        clip: true

        delegate: Rectangle {
            id: _dataDelegate

            width: ListView.view.width
            height: _text.paintedHeight
            color: "cornflowerblue"

            border {
                color: "lightskyblue"
                width: 2
            }

            anchors {
                bottom: _root.bottom
                bottomMargin:  ListView.view.height
            }

            Rectangle {
                width: 100
                height: _dataDelegate.height - 5
                color: "crimson"
                radius: 10

                anchors {
                    top: parent.top
                    topMargin: 2
                    right: parent.right
                    rightMargin: 10
                }

                Text {
                    text: "Remove"
                    font.pointSize: 18
                    color: "white"
                    anchors.centerIn: parent
                }

                MouseArea
                {
                    anchors.fill: parent
                    onClicked: {
                        _weatherModel.remove(index)
                    }
                }
            }

            Text {
                id: _text
                text: name
                color: "white"

                anchors.left: parent.left
                font.pointSize: 20
                horizontalAlignment: Text.AlignHCenter

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        _dataDelegate.ListView.view.clicked(index);
                    }
                }
            }
        }

        ScrollBar.vertical: ScrollBar {}
    }

    Component.onCompleted: {
        _listView.clicked(0);
    }
}
